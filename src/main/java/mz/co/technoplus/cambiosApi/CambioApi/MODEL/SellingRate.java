package mz.co.technoplus.cambiosApi.CambioApi.MODEL;

import javax.persistence.*;

@Entity
public class SellingRate {
    @Id
    private Long id;
//    @OneToOne
//    private Currency currency;
    private  String currency;
    private Double value;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SellingRate{" +
                "id=" + id +
                ", currency='" + currency + '\'' +
                ", value=" + value +
                '}';
    }
}
