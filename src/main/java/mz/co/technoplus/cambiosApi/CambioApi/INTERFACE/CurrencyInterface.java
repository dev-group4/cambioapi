package mz.co.technoplus.cambiosApi.CambioApi.INTERFACE;

import mz.co.technoplus.cambiosApi.CambioApi.MODEL.Currency;

import java.util.List;

public interface CurrencyInterface {
    <P extends Currency> P save(P save);
    List<Currency> getCurrency();
}
