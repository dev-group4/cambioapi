package mz.co.technoplus.cambiosApi.CambioApi;

import mz.co.technoplus.cambiosApi.CambioApi.SERVICES.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author Abrantes Cua
 * July 2022
 * expose rate REST API using H2 Database for persistance
 */
@SpringBootApplication
public class CambioApiApplication {


	public static void main(String[] args) {
		SpringApplication.run(CambioApiApplication.class, args);

	}

}
