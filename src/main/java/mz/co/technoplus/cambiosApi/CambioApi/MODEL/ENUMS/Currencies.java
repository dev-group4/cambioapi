package mz.co.technoplus.cambiosApi.CambioApi.MODEL.ENUMS;

public enum Currencies {
    MZN,
    USD,
    ZAR,
    EUR,
    ANG,
    AOA,
    ARS,
    AUD,
    AWG,
    AZN,
    BAM,
    BBD,
    BDT,
    BGN,
    BHD,
    BIF

}
