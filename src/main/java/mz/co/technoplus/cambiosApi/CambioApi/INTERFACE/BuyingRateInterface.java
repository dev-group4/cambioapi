package mz.co.technoplus.cambiosApi.CambioApi.INTERFACE;

import mz.co.technoplus.cambiosApi.CambioApi.MODEL.BuyingRate;

public interface BuyingRateInterface {
    <P extends BuyingRate> P save(P save);

}
