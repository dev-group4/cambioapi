package mz.co.technoplus.cambiosApi.CambioApi.INTERFACE;

import mz.co.technoplus.cambiosApi.CambioApi.MODEL.CambioModel;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.Currency;

import java.util.List;

public interface CambioInterface {
    <P extends CambioModel> P save(P save);
    List<CambioModel> getCambio(String currency);
}
