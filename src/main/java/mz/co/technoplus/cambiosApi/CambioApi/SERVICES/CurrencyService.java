package mz.co.technoplus.cambiosApi.CambioApi.SERVICES;

import mz.co.technoplus.cambiosApi.CambioApi.INTERFACE.CurrencyInterface;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.Currency;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.ENUMS.Currencies;
import mz.co.technoplus.cambiosApi.CambioApi.REPOSITORY.CurrencyRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CurrencyService {
    @Autowired
    private CurrencyInterface currencyInterface;
    @Autowired
    private CurrencyRepository currencyRepository;
    private static final Logger LOGGER = LogManager.getLogger(CurrencyService.class);

    public  void Loading_currencies(){
        LOGGER.info("Populating the currency table");
        Currency currency1 = new Currency();
        currency1.setId(1);
        currency1.setName(Currencies.MZN.toString());
        currencyInterface.save(currency1);
        LOGGER.info("Currency 1 saved {}",currency1);

        Currency currency2 = new Currency();
        currency2.setId(2);
        currency2.setName(Currencies.USD.toString());
        currencyInterface.save(currency2);
        LOGGER.info("Currency 2 saved {}",currency2);

        Currency currency3 = new Currency();
        currency3.setId(3);
        currency3.setName(Currencies.ZAR.toString());
        currencyInterface.save(currency3);
        LOGGER.info("Currency 3 saved {}",currency3);

        Currency currency4 = new Currency();
        currency4.setId(4);
        currency4.setName(Currencies.ANG.toString());
        currencyInterface.save(currency4);
        LOGGER.info("Currency 3 saved {} ",currency4);


    }



}
