package mz.co.technoplus.cambiosApi.CambioApi.REPOSITORY;

import mz.co.technoplus.cambiosApi.CambioApi.INTERFACE.CambioInterface;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.CambioModel;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CambioRepository extends JpaRepository<CambioModel, String>, CambioInterface {
    @Query(value = "SELECT * FROM CAMBIO_MODEL  p where p.base_currency = ?1", nativeQuery = true)
    List<CambioModel> getCambio(String currency);
}
