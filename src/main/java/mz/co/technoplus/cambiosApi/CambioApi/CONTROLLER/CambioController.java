package mz.co.technoplus.cambiosApi.CambioApi.CONTROLLER;

import mz.co.technoplus.cambiosApi.CambioApi.INTERFACE.CambioInterface;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.CambioModel;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.Currency;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.RequestModel;
import mz.co.technoplus.cambiosApi.CambioApi.REPOSITORY.CambioRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.util.List;

@RestController
@RequestMapping("/consulta")

public class CambioController {
    @Autowired
    private CambioInterface cambioInterface;
    private static final Logger LOGGER = LogManager.getLogger(CambioController.class);

    @GetMapping("cambio")
    public ResponseEntity<CambioModel>  Consulta_Cambio(@RequestBody RequestModel requestModel) {
        Response response = null;
        CambioModel cambioModel=null;
        LOGGER.info("Is being submitted a request for  base currency {}", requestModel.getBase_currency());
        List<CambioModel> cambio = cambioInterface.getCambio(requestModel.getBase_currency());
        for (CambioModel p : cambio) {
            cambioModel=p;
        }
        try {
                if(requestModel.getBase_currency().isEmpty()){
                    LOGGER.info("The field base_currency is mandatory");
                }else{
                    LOGGER.info("got the following result {}", cambio);
                }
        } catch (Exception e) {
            LOGGER.error("Error while receiving payment. Cause -> {}", e.getMessage());
        }
        return ResponseEntity.ok(cambioModel);
    }

}
