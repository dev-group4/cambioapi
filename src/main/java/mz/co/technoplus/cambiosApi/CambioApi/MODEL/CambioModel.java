package mz.co.technoplus.cambiosApi.CambioApi.MODEL;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class CambioModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private  String base_currency;
    private Date last_updated;
    private String source_information;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cambioModel_id")
    private List<BuyingRate> buyingRate = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cambioModel_id")
    private List<SellingRate> sellingRate = new ArrayList<>();

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getBase_currency() {
        return base_currency;
    }

    public void setBase_currency(String base_currency) {
        this.base_currency = base_currency;
    }

    public Date getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(Date last_updated) {
        this.last_updated = last_updated;
    }

    public String getSource_information() {
        return source_information;
    }

    public void setSource_information(String source_information) {
        this.source_information = source_information;
    }

    public List<BuyingRate> getBuyingRate() {
        return buyingRate;
    }

    public void setBuyingRate(List<BuyingRate> buyingRate) {
        this.buyingRate = buyingRate;
    }

    public List<SellingRate> getSellingRate() {
        return sellingRate;
    }

    public void setSellingRate(List<SellingRate> sellingRate) {
        this.sellingRate = sellingRate;
    }

    @Override
    public String toString() {
        return "CambioModel{" +
                "Id=" + Id +
                ", base_currency='" + base_currency + '\'' +
                ", last_updated=" + last_updated +
                ", source_information='" + source_information + '\'' +
                ", buyingRate=" + buyingRate +
                ", sellingRate=" + sellingRate +
                '}';
    }
}
