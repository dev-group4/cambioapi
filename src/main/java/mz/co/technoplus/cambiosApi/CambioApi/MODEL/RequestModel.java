package mz.co.technoplus.cambiosApi.CambioApi.MODEL;

public class RequestModel {

    private String base_currency;
    private String language;

    public String getBase_currency() {
        return base_currency;
    }

    public void setBase_currency(String base_currency) {
        this.base_currency = base_currency;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "RequestModel{" +
                "base_currency='" + base_currency + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}
