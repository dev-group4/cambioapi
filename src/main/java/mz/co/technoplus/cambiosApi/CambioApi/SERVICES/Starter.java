package mz.co.technoplus.cambiosApi.CambioApi.SERVICES;

import mz.co.technoplus.cambiosApi.CambioApi.MODEL.Currency;
import mz.co.technoplus.cambiosApi.CambioApi.REPOSITORY.CurrencyRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Starter {
    @Autowired
    private CambioService cambioService;
    @Autowired
    private CurrencyService currencyService;
    @Autowired
    private CurrencyRepository currencyRepository;
    private static final Logger LOGGER = LogManager.getLogger(Starter.class);

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        LOGGER.info("ATTEMPTING TO RUN THE SERVICES FOR POPULATING THE DATABASE");

        currencyService.Loading_currencies();
        cambioService.Loading_rates();

    }
}
