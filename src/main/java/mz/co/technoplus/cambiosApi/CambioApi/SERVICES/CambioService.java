package mz.co.technoplus.cambiosApi.CambioApi.SERVICES;

import mz.co.technoplus.cambiosApi.CambioApi.INTERFACE.CambioInterface;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.BuyingRate;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.CambioModel;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.Currency;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.ENUMS.Currencies;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.SellingRate;
import mz.co.technoplus.cambiosApi.CambioApi.REPOSITORY.CambioRepository;
import mz.co.technoplus.cambiosApi.CambioApi.REPOSITORY.CurrencyRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class CambioService {
    @Autowired
    private CambioInterface cambioInterface;
    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private CambioRepository cambioRepository;

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger LOGGER = LogManager.getLogger(CambioService.class);
    @Transactional
    public void Loading_rates(){
        LOGGER.info("=>Attempting to populate rate table ");

        CambioModel cambioModel =new CambioModel();
        cambioModel.setId(1);
        cambioModel.setBase_currency(Currencies.USD.toString());
        cambioModel.setLast_updated(new Date());
        cambioModel.setSource_information("https://www.bancomoc.mz/fm_mercadosmmi.aspx?id=10");


//        Setting buying values for each currency in accord with the base currency
        List<Currency> currency = currencyRepository.getCurrency();
        LOGGER.info("Object size {}",currency.size());
        int counter=1;

//        Looping into currency table to apply buying rate ammount or value
        for (Currency p : currency) {
            BuyingRate buyingRate = new BuyingRate();
            buyingRate.setCurrency(p.getName());
            buyingRate.setValue(100.0);
            buyingRate.setId(Long.parseLong(String.valueOf(counter)));
            cambioModel.getBuyingRate().add(buyingRate);
            counter++;
        }

//         Looping into currency table to apply  selling rate ammount or value
        for (Currency p : currency) {
            SellingRate sellingRate = new SellingRate();
            sellingRate.setCurrency(p.getName());
            sellingRate.setValue(30.0);
            sellingRate.setId(Long.parseLong(String.valueOf(counter)));
            cambioModel.getSellingRate().add(sellingRate);
            counter++;
        }



        cambioInterface.save(cambioModel);
        LOGGER.info("Object saved => {}", cambioModel);

//SECOND INSERTION
        CambioModel cambioModel1 =new CambioModel();
        cambioModel1.setId(2);
        cambioModel1.setBase_currency(Currencies.MZN.toString());
        cambioModel1.setLast_updated(new Date());
        cambioModel1.setSource_information("https://www.bancomoc.mz/fm_mercadosmmi.aspx?id=10");


//        Setting buying values for each currency in accord with the base currency
        List<Currency> currency1 = currencyRepository.getCurrency();
        LOGGER.info("Object size {}",currency1.size());
        int counter1=1;

//        Looping into currency table to apply buying rate ammount or value
        for (Currency p : currency) {
            BuyingRate buyingRate = new BuyingRate();
            buyingRate.setCurrency(p.getName());
            buyingRate.setValue(59.5);
            buyingRate.setId(Long.parseLong(String.valueOf(counter)));
            cambioModel1.getBuyingRate().add(buyingRate);
            counter++;
        }

//         Looping into currency table to apply  selling rate ammount or value
        for (Currency p : currency) {
            SellingRate sellingRate = new SellingRate();
            sellingRate.setCurrency(p.getName());
            sellingRate.setValue(40.0);
            sellingRate.setId(Long.parseLong(String.valueOf(counter)));
            cambioModel1.getSellingRate().add(sellingRate);
            counter++;
        }



        cambioInterface.save(cambioModel1);
        LOGGER.info("Object saved => {}", cambioModel1);

    }




}
