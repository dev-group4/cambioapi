package mz.co.technoplus.cambiosApi.CambioApi.INTERFACE;


import mz.co.technoplus.cambiosApi.CambioApi.MODEL.SellingRate;

public interface SellingRateInterface {
    <P extends SellingRate> P save(P save);

}
