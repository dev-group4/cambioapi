package mz.co.technoplus.cambiosApi.CambioApi.REPOSITORY;

import mz.co.technoplus.cambiosApi.CambioApi.INTERFACE.CurrencyInterface;
import mz.co.technoplus.cambiosApi.CambioApi.MODEL.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CurrencyRepository extends JpaRepository<Currency, String>, CurrencyInterface {

    @Query(value = "SELECT * FROM Currency p", nativeQuery = true)
    List<Currency> getCurrency();
}
